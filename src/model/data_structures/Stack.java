package model.data_structures;

import java.util.Iterator;

public class Stack<T extends Comparable<T>> implements IStack<T> {

	//-----------------------------------------------------------
	// Node class
	//-----------------------------------------------------------

	private class Node {

		//-----------------------------------------------------------
		// Attributes
		//-----------------------------------------------------------

		T element;

		Node next;

		//-----------------------------------------------------------
		// Constructor
		//-----------------------------------------------------------

		/**
		 * @param element
		 * @param next
		 */
		public Node(T element) {
			this.element = element;
			this.next = null;
		}

		//-----------------------------------------------------------
		// Methods
		//-----------------------------------------------------------

		/**
		 * @return the element
		 */
		public T getElement() {
			return element;
		}

		/**
		 * @return the next
		 */
		public Node getNext() {
			return next;
		}

		/**
		 * @param next the next to set
		 */
		public void setNext(Node next) {
			this.next = next;
		}
	}

	//-----------------------------------------------------------
	// Attributes
	//-----------------------------------------------------------

	private Node firstNode;

	private int listSize;

	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------

	public Stack() {
		firstNode = null;
		listSize = 0;
	}

	//-----------------------------------------------------------
	// Methods
	//-----------------------------------------------------------

	@Override
	public Iterator<T> iterator() {
		return new Iterator<T>() {
			Node act = null;

			@Override
			public boolean hasNext() {
				if (listSize == 0) {
					return false;
				}

				if (act == null) {
					return true;
				}

				return act.getNext() != null;
			}

			@Override
			public T next() {
				if (act == null) {
					act = firstNode;
				} else {
					act = act.getNext();
				}

				return act.getElement();
			}
		};
	}

	@Override
	public boolean isEmpty() {
		return firstNode == null;
	}

	@Override
	public int size() {
		return listSize;
	}

	@Override
	public void push(T t) {
		Node oldFirst = firstNode;
		firstNode = new Node(t);

		firstNode.setNext(oldFirst);
		listSize ++;
	}

	@Override
	public T pop() {
		T element = null;
		
		if(!isEmpty()) {
			element = firstNode.getElement();
			firstNode = firstNode.getNext();
			
			listSize --;
		} else {
			return element;
		}

		return element;
	}

}
