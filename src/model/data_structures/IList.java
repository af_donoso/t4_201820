package model.data_structures;

import java.util.Iterator;

public interface IList<T extends Comparable<T>> extends Iterable<T> {
	public void add(T elem);

	public T remove(T elem);

	public int size();

	public T get(T elem);

	public T get(int pos);
	
	public void set(int pos, T elem);

	public boolean isEmpty();
	
	public Iterator<T> iterator();
}
