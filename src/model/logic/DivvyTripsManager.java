package model.logic;

import java.io.FileReader;
import java.time.LocalDateTime;
import java.util.Random;
import com.opencsv.CSVReader;

import api.IDivvyTripsManager;
import model.vo.VOTrip;
import view.DivvyTripsManagerView;
import model.data_structures.IList;
import model.data_structures.List;
import model.data_structures.Stack;
import model.logic.StdRandom;


public class DivvyTripsManager implements IDivvyTripsManager {

	private Stack<VOTrip> tripsStack = new Stack<>();

	public int getStackSize() {
		return tripsStack.size();
	}

	public int loadTrips (String tripsFile) {
		try {
			tripsStack = new Stack<>();

			CSVReader reader = new CSVReader(new FileReader(tripsFile));
			String[] nextLine;
			reader.readNext();

			while((nextLine = reader.readNext()) != null) {
				VOTrip trip = null;

				LocalDateTime startTime = DivvyTripsManagerView.convertirFecha_Hora_LDT(nextLine[1].split(" ")[0], nextLine[1].split(" ")[1]);
				LocalDateTime endTime = DivvyTripsManagerView.convertirFecha_Hora_LDT(nextLine[2].split(" ")[0], nextLine[2].split(" ")[1]);

				if(nextLine[10].isEmpty() && nextLine[11].isEmpty()) {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), startTime, endTime, 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], "", 0);

				} else if (nextLine[11].isEmpty()) {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), startTime, endTime, 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], nextLine[10], 0);

				} else {
					trip = new VOTrip(Integer.parseInt(nextLine[0]), startTime, endTime, 
							Integer.parseInt(nextLine[3]), Integer.parseInt(nextLine[4]), 
							Integer.parseInt(nextLine[5]), nextLine[6], Integer.parseInt(nextLine[7]), 
							nextLine[8], nextLine[9], nextLine[10], Integer.parseInt(nextLine[11]));
				}

				tripsStack.push(trip);
			}

			reader.close();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		return tripsStack.size();
	}

	@Override
	public IList<VOTrip> muestraDeViajes(int n) {
		List<VOTrip> list = new List<>();

		for(int i = 0; i < n; i++) {
			list.add(tripsStack.pop());
		}

		System.out.println("Quedan " + tripsStack.size() + " viajes en la pila.");
		return list;
	}

	@Override
	public <T extends Comparable<T>> void listaOrdenamientoShellsort(List<T> list) {
		long startTime = System.currentTimeMillis();

		int N = list.size();

		int h = 1;
		while(h < N/3) h = 3*h +1;

		while(h >= 1) {
			for(int i = h; i < N; i++) {
				for(int j = i; j >= h && list.get(j).compareTo(list.get(j-h)) > 0; j -= h) {
					T swap = list.get(j);
					list.set(j, list.get(j-h));
					list.set(j-h, swap);
				}
			}

			h = h/3;
		}

		long stopTime = System.currentTimeMillis();
		long timeTaken = stopTime - startTime;
		System.out.println("Shell Sort: " + timeTaken);
	}
	private static <T extends Comparable<T>> int partition(List<T> list, int lo, int hi)
	{
		int i = lo, j = hi+1;
		while (true)
		{
			while (list.get(++i).compareTo(list.get(lo))>0)
				if (i == hi) break;
			while (list.get(lo).compareTo(list.get(--j))>0)
				if (j == lo) break;

			if (i >= j) break;
			T temp= list.get(i);
			list.set(i, list.get(j));
			list.set(j, temp);
		}
		T tempp= list.get(lo);
		list.set(lo, list.get(j));
		list.set(j, tempp);
		return j;
	}
	public <T extends Comparable <T>> void listaOrdenamientoQuickSort(List<T> list)
	{
		long startTime = System.currentTimeMillis();
		
		StdRandom.shuffle(list);
		sort(list, 0, list.size()-1);
		
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
	}
	
	private <T extends Comparable<T>> void sort(List<T> a, int lo, int hi)
	{
		if (hi <= lo) return;
		int j = partition(a, lo, hi);
		sort(a, lo, j-1);
		sort(a, j+1, hi);

	}
}

