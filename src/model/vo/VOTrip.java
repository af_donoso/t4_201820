package model.vo;

import java.time.LocalDateTime;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable<VOTrip>{
	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	private int id;

	private LocalDateTime startTime;

	private LocalDateTime endTime;

	private int bikeId;

	private int tripDuration;

	private int fromStationId;

	private String fromStationName;

	private int toStationId;

	private String toStationName;

	private String userType;

	private String gender;

	private int birthYear;

	//-----------------------------------------------------------
	// Constructor
	//-----------------------------------------------------------

	/**
	 * @param id
	 * @param startTime
	 * @param endTime
	 * @param bikeId
	 * @param tripDuration
	 * @param fromStationId
	 * @param fromStationName
	 * @param toStationId
	 * @param toStationName
	 * @param userType
	 * @param gender
	 * @param birthYear
	 */
	public VOTrip(int id, LocalDateTime startTime, LocalDateTime endTime, int bikeId, int tripDuration, int fromStationId,
			String fromStationName, int toStationId, String toStationName, String userType, String gender,
			int birthYear) {
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.bikeId = bikeId;
		this.tripDuration = tripDuration;
		this.fromStationId = fromStationId;
		this.fromStationName = fromStationName;
		this.toStationId = toStationId;
		this.toStationName = toStationName;
		this.userType = userType;
		this.gender = gender;
		this.birthYear = birthYear;
	}

	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the startTime
	 */
	public LocalDateTime getStartTime() {
		return startTime;
	}

	/**
	 * @return the endTime
	 */
	public LocalDateTime getEndTime() {
		return endTime;
	}

	/**
	 * @return the bikeId
	 */
	public int getBikeId() {
		return bikeId;
	}

	/**
	 * @return the tripDuration
	 */
	public int getTripDuration() {
		return tripDuration;
	}

	/**
	 * @return the fromStationId
	 */
	public int getFromStationId() {
		return fromStationId;
	}

	/**
	 * @return the fromStationName
	 */
	public String getFromStationName() {
		return fromStationName;
	}

	/**
	 * @return the toStationId
	 */
	public int getToStationId() {
		return toStationId;
	}

	/**
	 * @return the toStationName
	 */
	public String getToStationName() {
		return toStationName;
	}

	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @return the birthYear
	 */
	public int getBirthYear() {
		return birthYear;
	}

	@Override
	public int compareTo(VOTrip o) {
		if(o.getBikeId() == bikeId) {
			
			if(o.getStartTime().compareTo(startTime) > 0) {
				return 1;
			} else if (o.getStartTime().compareTo(startTime) < 0) {
				return -1;
			} else {
				return 0;
			}
			
		} else if (o.getBikeId() > bikeId) {
			return 1;
		} else {
			return -1;
		}
	}
}
