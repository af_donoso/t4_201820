package model.vo;

/**
 * Representation of a bike object
 */
public class VOBike implements Comparable<VOBike>{

	//-----------------------------------------------------------
		// Atributos
		//-----------------------------------------------------------

		private int id;

		private String name;

		private String city;

		private double latitude;

		private double longitude;

		private int dpCapacity;

		private String date;

		//-----------------------------------------------------------
		// Constructor
		//-----------------------------------------------------------

		/**
		 * @param id
		 * @param name
		 * @param city
		 * @param latitude
		 * @param longitude
		 * @param dpCapacity
		 * @param date
		 */
		public VOBike(int id, String name, String city, double latitude, double longitude, int dpCapacity, String date) {
			this.id = id;
			this.name = name;
			this.city = city;
			this.latitude = latitude;
			this.longitude = longitude;
			this.dpCapacity = dpCapacity;
			this.date = date;
		}

		//-----------------------------------------------------------
		// Métodos
		//-----------------------------------------------------------

		/**
		 * @return the id
		 */
		public int getId() {
			return id;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @return the city
		 */
		public String getCity() {
			return city;
		}

		/**
		 * @return the latitude
		 */
		public double getLatitude() {
			return latitude;
		}

		/**
		 * @return the longitude
		 */
		public double getLongitude() {
			return longitude;
		}

		/**
		 * @return the dpCapacity
		 */
		public int getDpCapacity() {
			return dpCapacity;
		}

		/**
		 * @return the date
		 */
		public String getDate() {
			return date;
		}

		/**
		 * @param id the id to set
		 */
		public void setId(int id) {
			this.id = id;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * @param city the city to set
		 */
		public void setCity(String city) {
			this.city = city;
		}

		/**
		 * @param latitude the latitude to set
		 */
		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}

		/**
		 * @param longitude the longitude to set
		 */
		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}

		/**
		 * @param dpCapacity the dpCapacity to set
		 */
		public void setDpCapacity(int dpCapacity) {
			this.dpCapacity = dpCapacity;
		}

		/**
		 * @param date the date to set
		 */
		public void setDate(String date) {
			this.date = date;
		}

		@Override
		public int compareTo(VOBike o) {
			if(o.getId() == id) {
				return 0;
			} else if (o.getId() > id) {
				return 1;
			} else {
				return -1;
			}
		}
}
