package view;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.List;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class DivvyTripsManagerView 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin)
		{
			printMenu();

			int option = sc.nextInt();
			int number = 0;
			int n = 0;

			switch(option)
			{


				case 1:
					System.out.println("Se cargaron " + Controller.loadTrips() + " viajes.");
					break;
	
				case 2:
					printDataNumberOptions();
					number= sc.nextInt();
					n = 0;
	
					switch(number) {
	
						case 1:
							n = 10;
							break;
		
						case 2:
							n = 50;
							break;
		
						case 3:
							n = 100;
							break;
		
						case 4:
							n = 1000;
							break;
		
						case 5:
							n = 5000;
							break;
		
						case 6:
							n = 10000;
							break;
		
						case 7:
							n = 50000;
							break;
		
						case 8:
							n = 100000;
							break;
		
						case 9:
							n = 500000;
							break;
		
						case 10:
							n = Controller.getStackSize();
							break;
		
						default:
							System.out.println("Not a valid option");
							break;
					}
	
					List<VOTrip> listShell = (List<VOTrip>) Controller.muestraDeViajes(n);
					/*Iterator<VOTrip> iter = list.iterator();
	
					System.out.println("ANTES:");
					while(iter.hasNext()) {
						VOTrip trip = iter.next();
						System.out.println(trip.getBikeId());
					}*/
	
					Controller.listaOrdenamientoShellsort(listShell);
					
					/*Iterator<VOTrip> iter = listShell.iterator();
	
					System.out.println("DESPUES:");
					while(iter.hasNext()) {
						VOTrip trip = iter.next();
						System.out.println(trip.getBikeId() + " " + trip.getStartTime());
					}*/
	
					break;
					
				case 3:
					printDataNumberOptions();
					number= sc.nextInt();
					n = 0;
	
					switch(number) {
	
						case 1:
							n = 10;
							break;
		
						case 2:
							n = 50;
							break;
		
						case 3:
							n = 100;
							break;
		
						case 4:
							n = 1000;
							break;
		
						case 5:
							n = 5000;
							break;
		
						case 6:
							n = 10000;
							break;
		
						case 7:
							n = 50000;
							break;
		
						case 8:
							n = 100000;
							break;
		
						case 9:
							n = 500000;
							break;
		
						case 10:
							n = Controller.getStackSize();
							break;
		
						default:
							System.out.println("Not a valid option");
							break;
					}
					List<VOTrip> listQuick = (List<VOTrip>) Controller.muestraDeViajes(n);
					
					Controller.listaOrdenadaQuickSort(listQuick);
					
					break;
	
				case 4:	
					fin=true;
					sc.close();
					break;
					
				default:
					System.out.println("La opción no es válida");
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 4----------------------");
		System.out.println("1. Cree una nueva coleccion de viajes");
		System.out.println("2. Ordenar utilizando Shell Sort");
		System.out.println("3. Ordenar utilizando Quick Sort");
		System.out.println("4. Salir");
		System.out.println("Digite el n�mero de opci�n para ejecutar la tarea, luego presione enter: (Ej., 1):");
	}

	private static void printDataNumberOptions() {
		System.out.println("Seleccione el número de datos que desea cargar, luego presione enter (Ej. 1):");
		System.out.println("1. 10");
		System.out.println("2. 50");
		System.out.println("3. 100");
		System.out.println("4. 1000");
		System.out.println("5. 5000");
		System.out.println("6. 10000");
		System.out.println("7. 50000");
		System.out.println("8. 100000");
		System.out.println("9. 500000");
		System.out.println("10. Todos los datos");
	}

	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	public static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[0]);
		int dia = Integer.parseInt(datosFecha[1]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = Integer.parseInt(datosHora[2]);

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}
}
