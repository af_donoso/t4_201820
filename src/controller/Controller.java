package controller;

import api.IDivvyTripsManager;
import model.data_structures.IList;
import model.data_structures.List;
import model.logic.DivvyTripsManager;
import model.vo.VOTrip;

public class Controller 
{
	public final static String TRIPS_FILE = "./data/Divvy_Trips_2017_Q2.csv";

	/**
	 * Reference to the services manager
	 */
	@SuppressWarnings("unused")
	private static IDivvyTripsManager  manager = new DivvyTripsManager();

	public static int getStackSize() {
		return manager.getStackSize();
	}

	public static int loadTrips() 
	{
		return manager.loadTrips(TRIPS_FILE);

	}
	public static IList<VOTrip> muestraDeViajes (int n) 
	{
		return manager.muestraDeViajes(n);
	}
	public static void listaOrdenamientoShellsort(List<VOTrip> list)
	{
		manager.listaOrdenamientoShellsort(list);
	}
	public static void listaOrdenadaQuickSort(List<VOTrip> list)
	{
		manager.listaOrdenamientoQuickSort(list);
	}

}
