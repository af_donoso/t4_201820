package api;

import model.data_structures.IList;
import model.data_structures.List;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IDivvyTripsManager {

	int getStackSize();

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */
	int loadTrips(String tripsFile);
	/**
	 * Method to load the Divvy trips
	 * @param tripsFile
	 */
	IList<VOTrip> muestraDeViajes(int n);
	/**
	 * 
	 */
	<T extends Comparable<T>> void listaOrdenamientoShellsort(List<T> list);
	/**
	 * Metodo de ordenamiento MergeSort
	 */
	<T extends Comparable <T>> void listaOrdenamientoQuickSort(List<T> list); 
	
}
