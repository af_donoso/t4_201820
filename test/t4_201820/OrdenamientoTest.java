package t4_201820;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import controller.Controller;
import model.data_structures.List;
import model.vo.VOTrip;
import view.DivvyTripsManagerView;

class OrdenamientoTest {

	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	private List<VOTrip> list;

	//-----------------------------------------------------------
	// Scenarios
	//-----------------------------------------------------------

	private void scenario1() {
		list = new List<>();

		VOTrip trip1 = new VOTrip(0, DivvyTripsManagerView.convertirFecha_Hora_LDT("1/1/2018", "10:00:00"), DivvyTripsManagerView.convertirFecha_Hora_LDT("1/1/2018", "10:00:00"), 0, 0, 0, "", 0, "", "", "", 0);
		VOTrip trip2 = new VOTrip(0, DivvyTripsManagerView.convertirFecha_Hora_LDT("1/2/2018", "10:00:00"), DivvyTripsManagerView.convertirFecha_Hora_LDT("1/2/2018", "10:00:00"), 1, 0, 0, "", 0, "", "", "", 0);
		VOTrip trip3 = new VOTrip(0, DivvyTripsManagerView.convertirFecha_Hora_LDT("1/3/2018", "10:00:00"), DivvyTripsManagerView.convertirFecha_Hora_LDT("1/3/2018", "10:00:00"), 2, 0, 0, "", 0, "", "", "", 0);
		VOTrip trip4 = new VOTrip(0, DivvyTripsManagerView.convertirFecha_Hora_LDT("1/4/2018", "10:00:00"), DivvyTripsManagerView.convertirFecha_Hora_LDT("1/4/2018", "10:00:00"), 3, 0, 0, "", 0, "", "", "", 0);
		VOTrip trip5 = new VOTrip(0, DivvyTripsManagerView.convertirFecha_Hora_LDT("1/5/2018", "10:00:00"), DivvyTripsManagerView.convertirFecha_Hora_LDT("1/5/2018", "10:00:00"), 4, 0, 0, "", 0, "", "", "", 0);
		VOTrip trip6 = new VOTrip(0, DivvyTripsManagerView.convertirFecha_Hora_LDT("1/6/2018", "10:00:00"), DivvyTripsManagerView.convertirFecha_Hora_LDT("1/6/2018", "10:00:00"), 5, 0, 0, "", 0, "", "", "", 0);
		VOTrip trip7 = new VOTrip(0, DivvyTripsManagerView.convertirFecha_Hora_LDT("1/7/2018", "10:00:00"), DivvyTripsManagerView.convertirFecha_Hora_LDT("1/7/2018", "10:00:00"), 2, 0, 0, "", 0, "", "", "", 0);
		VOTrip trip8 = new VOTrip(0, DivvyTripsManagerView.convertirFecha_Hora_LDT("1/8/2018", "10:00:00"), DivvyTripsManagerView.convertirFecha_Hora_LDT("1/8/2018", "10:00:00"), 3, 0, 0, "", 0, "", "", "", 0);

		list.add(trip2);
		list.add(trip4);
		list.add(trip5);
		list.add(trip7);
		list.add(trip8);
		list.add(trip1);
		list.add(trip3);
		list.add(trip6);
	}

	//-----------------------------------------------------------
	// Atributos
	//-----------------------------------------------------------

	@Test
	void testListaOrdenamientoShellsort() {
		scenario1();
		Controller.listaOrdenamientoShellsort(list);

		for(int i = 0; i < list.size() - 1; i++) {
			System.out.println(list.get(i).getBikeId());

			if(list.get(i).compareTo(list.get(i + 1)) < 0) {
				fail("La lista no está ordenada.");
			}
		}
	}

	@Test
	void testListaOrdenamientoQuicksort() 
	{
		scenario1();
		Controller.listaOrdenadaQuickSort(list);

		for(int i = 0; i < list.size() - 1; i++) {
			System.out.println(list.get(i).getBikeId());

			if(list.get(i).compareTo(list.get(i + 1)) < 0) {
				fail("La lista no está ordenada.");
			}

		}
	}
}
